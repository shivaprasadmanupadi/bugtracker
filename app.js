var express = require('express');
var path = require('path');
var cors = require('cors');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var templateEngine = require('ejs');
var session = require('express-session');

var config = require(path.join(__dirname, "application", "config", "config"));
var apiResponseHelper = require(path.join(__dirname, "application", "helpers", "apiResponseHelper"));
var apiRouter = require(path.join(__dirname, "application", "routes", "apiRouter"));
var portalRouter = require(path.join(__dirname, "application", "routes", "portalRouter"));

var app = express();
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.set('views', path.join(__dirname, 'application', 'templates'));
app.engine('html', templateEngine.renderFile);
app.set('view engine', 'html');

mongoose.connect(config.database.url, config.database.options);
mongoose.connection.on('connected', function ()
{
    console.log('Mongoose default connection open to ' + config.database.url);
});
mongoose.connection.on('error', function (err)
{
    console.log('Mongoose default connection error: ' + err);
    mongoose.disconnect();
});
mongoose.connection.on('disconnected', function ()
{
    console.log('Mongoose default connection disconnected');
    process.exit(0);
});
process.on('SIGINT', function ()
{
    mongoose.connection.close(function ()
    {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});

app.use('/resources', express.static(path.join(__dirname, 'application', 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'application', 'uploads')));

/*session should be instantiated after static routes*/
app.use(cookieParser(config.session.secret));
app.use(session(
    {
        secret: config.session.secret,
        resave: true,
        saveUninitialized: true
    }
));
/*end of sessions middleware*/

app.use('/api/v1', apiRouter);
app.use('/', portalRouter);

app.use(function (req, res, next)
{
    res.status(404).send(apiResponseHelper.setResponse(false, "Not Found"));
});
app.use(function (err, req, res, next)
{
    res.status(err.status | 500).send(apiResponseHelper.setResponse(false, err.message, err.stack));
});

var expressServer = app.listen(config.server.port, function ()
{
    console.log("server is up and running on port " + config.server.port + " in " + config.server.env + " mode");
});