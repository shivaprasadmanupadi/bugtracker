var gruntConfiguration = function (grunt)
{

    require('time-grunt')(grunt);
    require('load-grunt-config')(grunt,
        {
            data: {
                pkg: grunt.file.readJSON('package.json'),
            }
        });

    grunt.registerTask('dev', ['env:dev', 'exec:' + grunt.option('os') + '_startserver']);
    grunt.registerTask('prod', ['env:prod', 'exec:' + grunt.option('os') + '_startserver']);
};
module.exports = gruntConfiguration;