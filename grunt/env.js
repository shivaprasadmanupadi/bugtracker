var envConfig = {

    dev: {
        NODE_ENV: 'development',
        NODE_PORT: 8002
    },
    prod: {
        NODE_ENV: 'production',
        NODE_PORT: 3000
    }

};

module.exports = envConfig;