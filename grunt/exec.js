var execConfig = {

    windows_startserver: {
        cmd: "node app.js"
    },
    linux_startserver: {
        cmd: "pm2 start app.js"
    }

};

module.exports = execConfig;