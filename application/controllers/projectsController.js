var projectController = {};
var apiResponseHelper = require('../helpers/apiResponseHelper');
var config = require('../config/config');
var Projects = require('../models/project');
var utilsHelper = require('../helpers/utilsHelper');
var $q = require('q');
var bugsController = require('./bugsController');

/*create project*/
projectController.createProject = function (req, res, next)
{

    projectController._createProject_(req.params.workspaceid, req.body.name)
        .then(
        function (project)
        {
            res.status(200).send(apiResponseHelper.setResponse(true, "project created successfully", project));
        }, function (err)
        {
            utilsHelper.mongooseErrorCallback(err, res);
        });
};

projectController._createProject_ = function (workspaceid, name)
{
    var deferred = $q.defer();

    if (!workspaceid)
    {
        deferred.reject(utilsHelper.newError(400, 'invalid workspace'));
        return deferred.promise;
    }
    if (!name)
    {
        deferred.reject(utilsHelper.newError(400, 'invalid project name'));
        return deferred.promise;
    }

    projectController._getProject_({'workspace_id': workspaceid, 'name': name}).then(function (project)
    {
        deferred.reject(utilsHelper.newError(400, "project name " + project.name + " already exists"));
    }, function (err)
    {
        if (err.flag == 'projectnotexists')
        {
            new Projects({'name': name, 'workspace_id': workspaceid}).save(function (err, project)
            {
                if (err)
                {
                    deferred.reject(err);
                }
                else
                {
                    deferred.resolve(project);
                }
            });
        }
        else
        {
            deferred.reject(err);
        }
    });

    return deferred.promise;
};
/*end of create project*/

/*get all projects of workspace*/
projectController.getProjects = function (req, res, next)
{
    var query = {};
    query.workspace_id = req.params.workspaceid;
    projectController._getProjects_(query)
        .then(
        function (projects)
        {
            res.status(200).send(apiResponseHelper.setResponse(true, "projects listing", projects));
        }, function (err)
        {
            utilsHelper.mongooseErrorCallback(err, res);
        });
};

projectController._getProjects_ = function (query)
{
    var deferred = $q.defer();

    Projects.find(query, function (err, projects)
    {
        if (err)
        {
            deferred.reject(err);
        }
        else
        {
            deferred.resolve(projects);
        }
    });

    return deferred.promise;
};
/*end of get all projects of workspace*/

/*get project details*/
projectController.getProject = function (req, res, next)
{

    res.status(200).send(apiResponseHelper.setResponse(true, "projects details", req.project));
};

projectController._getProject_ = function (query)
{
    var deferred = $q.defer();
    Projects.findOne(query, function (err, project)
    {
        if (err)
        {
            deferred.reject(err);
        }
        else
        {
            if (project)
            {
                deferred.resolve(project);
            }
            else
            {
                deferred.reject(utilsHelper.newError(400, "project not exists", "", 'projectnotexists'));
            }
        }
    });

    return deferred.promise;
};
/*end of get project details*/

/*link user*/
projectController.linkUser = function (req, res, next)
{
    if (!req.params.userid)
    {
        res.status(400).send(apiResponseHelper.setResponse(false, 'invalid userid'));
        return;
    }

    if (!utilsHelper.isValidObjectId(req.params.userid))
    {
        res.status(400).send(apiResponseHelper.setResponse(true, "invalid user id " + req.params.userid));
        return;
    }

    projectController._updateProject_({
        '_id': req.params.projectid,
        'workspace_id': req.params.workspaceid
    }, {$addToSet: {users: req.params.userid}}).then(function (updatedCount)
    {
        res.status(200).send(apiResponseHelper.setResponse(true, 'linked successfully'));
    }, function (err)
    {
        utilsHelper.mongooseErrorCallback(err, res);
    });

};

/*end of link user*/

/*unlink user*/
projectController.unlinkUser = function (req, res, next)
{
    if (!req.params.userid)
    {
        res.status(400).send(apiResponseHelper.setResponse(false, 'invalid userid'));
        return;
    }

    if (!utilsHelper.isValidObjectId(req.params.userid))
    {
        res.status(400).send(apiResponseHelper.setResponse(true, "invalid user id " + req.params.userid));
        return;
    }

    if (!req.query.userid)
    {
        res.status(400).send(apiResponseHelper.setResponse(false, 'invalid new userid'));
        return;
    }

    if (!utilsHelper.isValidObjectId(req.query.userid))
    {
        res.status(400).send(apiResponseHelper.setResponse(true, "invalid new user id " + req.params.userid));
        return;
    }


    bugsController._updateBug_({'project_id':req.project._id,'perform_user':req.params.userid},{$set: {perform_user: req.query.userid}}).then(function(count)
    {
        bugsController._updateBug_({'project_id':req.project._id,'current_user':req.params.userid},{$set: {current_user: req.query.userid}}).then(function(count)
        {
            bugsController._updateBug_({'project_id':req.project._id,'should_be_verified_by':req.params.userid},{$set: {should_be_verified_by: req.query.userid}}).then(function(count)
            {
                projectController._updateProject_({
                    '_id': req.params.projectid,
                    'workspace_id': req.params.workspaceid
                }, {$pull: {users: req.params.userid}}).then(function (updatedCount)
                {
                    res.status(200).send(apiResponseHelper.setResponse(true, 'unlinked successfully'));
                }, function (err)
                {
                    utilsHelper.mongooseErrorCallback(err, res);
                });
            });
        });
    });

};

/*end of unlink user*/

projectController._updateProject_ = function (query, updateQuery)
{
    var deferred = $q.defer();

    Projects.update(query, updateQuery, function (err, updatedCount)
    {
        if (err)
        {
            deferred.reject(err);
        }
        else
        {
            if (updatedCount)
            {
                deferred.resolve(updatedCount);
            }
            else
            {
                deferred.reject(utilsHelper.newError(400, 'invalid projectid'));
            }
        }
    });

    return deferred.promise;
};

/*get project users*/
projectController.getProjectUsers = function (req, res, next)
{
    projectController._getProject_({
        '_id': req.params.projectid,
        'workspace_id': req.params.workspaceid
    }).then(function (project)
    {
        project.populate('users', function (err, project)
        {
            res.status(200).send(apiResponseHelper.setResponse(true, 'project users', project.users));
        });
    }, function (err)
    {
        utilsHelper.mongooseErrorCallback(err, res);
    });
};

/*end of get project users*/

/*add category*/
projectController.addCategory = function(req, res,next)
{
    if (!req.body.name)
    {
        res.status(400).send(apiResponseHelper.setResponse(false, 'invalid name'));
        return;
    }

    req.project.update({$push:{"settings.categories":{name:req.body.name}}},function(err,updatedCount)
    {
        if(err)
        {
            utilsHelper.mongooseErrorCallback(err, res);
        }
        else
        {
            res.status(200).send(apiResponseHelper.setResponse(true, 'category added successfully'));
        }
    });
};
/*end of add category*/

/*add addrelease*/
projectController.addRelease = function(req, res,next)
{
    if (!req.body.name)
    {
        res.status(400).send(apiResponseHelper.setResponse(false, 'invalid name'));
        return;
    }

    if (!req.body.date)
    {
        res.status(400).send(apiResponseHelper.setResponse(false, 'invalid date'));
        return;
    }

    req.project.update({$push:{"settings.releases":req.body}},function(err,updatedCount)
    {
        if(err)
        {
            utilsHelper.mongooseErrorCallback(err, res);
        }
        else
        {
            res.status(200).send(apiResponseHelper.setResponse(true, 'release added successfully'));
        }
    });
};
/*end of add addrelease*/

/*add configuration*/
projectController.updateConfiguration = function(req, res,next)
{
    req.project.update({'settings.config.enable_categories':req.body.enable_categories,'settings.config.mandate_category_selection':req.body.mandate_category_selection,'settings.config.enable_releases':req.body.enable_releases,'settings.config.mandate_release_selection':req.body.mandate_release_selection},function(err,updatedCount)
    {
        if(err)
        {
            utilsHelper.mongooseErrorCallback(err, res);
        }
        else
        {
            res.status(200).send(apiResponseHelper.setResponse(true, 'configuration updated successfully'));
        }
    });
};
/*end of add configuration*/

module.exports = projectController;