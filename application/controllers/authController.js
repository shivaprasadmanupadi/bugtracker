var authController = {};
var apiResponseHelper = require('../helpers/apiResponseHelper');
var jwt = require('jsonwebtoken');
var config = require('../config/config');
var userController = require('./userController');
var utilsHelper = require('../helpers/utilsHelper');

authController.login = function (req, res, next)
{
	if (!req.body.workspaceid)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid workspace id'));
		return;
	}

	if (!req.body.email)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid email id'));
		return;
	}
	if (!req.body.password)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid password'));
		return;
	}

	if (!utilsHelper.isValidObjectId(req.body.workspaceid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid workspace id'));
		return;
	}

	userController._getUser_({'workspace_id': req.body.workspaceid, 'email': req.body.email}).then(function (user)
	{
		if (user.validatePassword(req.body.password))
		{
			req.session.authToken = utilsHelper.encrypt(jwt.sign({'user': user}, config.jwt.secret, config.jwt.options));//store token in session
			res.status(200).send(apiResponseHelper.setResponse(true, "logged in successfully", user));
		}
		else
		{
			res.status(401).send(apiResponseHelper.setResponse(false, "invalid credentials"));
		}
	}, function (err)
	{
		utilsHelper.mongooseErrorCallback(err, res);
	});

};

module.exports = authController;