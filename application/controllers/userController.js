var userController = {};

var apiResponseHelper = require('../helpers/apiResponseHelper');
var utilsHelper = require('../helpers/utilsHelper');
var config = require('../config/config');
var User = require('../models/user');
var $q = require('q');
var bugsController = require('./bugsController');
var projectsController = require('./projectsController');
var emailService = require('../helpers/emailService');

userController.createUser = function (req, res, next)
{
	userController._createUser_(req.params.workspaceid, req.body.name, req.body.email, req.body.password, req.body.role).then(function (user)
	{
		res.status(200).send(apiResponseHelper.setResponse(true, 'user added successfully', user));
	}, function (err)
	{
		utilsHelper.mongooseErrorCallback(err, res);
	});
};

userController._createUser_ = function (workspaceid, name, email, password, role)
{
	var deferred = $q.defer();

	if (!workspaceid)
	{
		deferred.reject(utilsHelper.newError(400, "invalid workspace id"));
		return deferred.promise;
	}
	if (!name)
	{
		deferred.reject(utilsHelper.newError(400, "invalid name"));
		return deferred.promise;
	}
	if (!email)
	{
		deferred.reject(utilsHelper.newError(400, "invalid email"));
		return deferred.promise;
	}
	if (!password)
	{
		deferred.reject(utilsHelper.newError(400, "invalid password"));
		return deferred.promise;
	}
	if (!role)
	{
		deferred.reject(utilsHelper.newError(400, "invalid role"));
		return deferred.promise;
	}

	userController._getUser_({'workspace_id': workspaceid, 'email': email}).then(function (user)
	{
		deferred.reject(utilsHelper.newError(400, 'user with email id :' + email + ' already exists'));
	}, function (err)
	{
		if (err.flag == 'nouserfound')
		{
			var newUser = {};
			newUser.name = name;
			newUser.email = email;
			newUser.password = utilsHelper.generateHash(password);
			newUser.workspace_id = workspaceid;
			newUser.role = role;
			new User(newUser).save(function (err, user)
			{
				if (err)
				{
					deferred.reject(err);
				}
				else
				{
					deferred.resolve(user);
				}
			});
		}
		else
		{
			deferred.reject(err);
		}
	});

	return deferred.promise;
};

userController.getUser = function (req, res, next)
{
	if (!req.params.userid)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid user id'));
		return;
	}
	if (!utilsHelper.isValidObjectId(req.params.userid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid user id'));
		return;
	}

	userController._getUser_({'_id': req.params.userid, 'workspace_id': req.params.workspaceid}).then(function (user)
	{
		console.log(req.query);
		if(req.query.projects == 'true')
		{
			projectsController._getProjects_({'users': req.params.userid, 'workspace_id': req.params.workspaceid}).then(function (projects)
			{
				console.log(1);
				user.set( 'projects',projects, { strict: false });
				res.status(200).send(apiResponseHelper.setResponse(true, 'user details', user));
			}, function (err)
			{
				utilsHelper.mongooseErrorCallback(err, res);
			});
		}
		else
		{
			console.log(2);
			res.status(200).send(apiResponseHelper.setResponse(true, 'user details', user));
		}
	}, function (err)
	{
		utilsHelper.mongooseErrorCallback(err, res);
	});
};

userController.getUserBugs = function (req, res, next)
{
	if (!req.params.userid)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid user id'));
		return;
	}
	if (!utilsHelper.isValidObjectId(req.params.userid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid user id'));
		return;
	}

	var query = {};
	query.current_user = req.params.userid;

	bugsController._getProjectBugs_(query)
		.then(
		function (bugs)
		{
			res.status(200).send(apiResponseHelper.setResponse(true, "bugs listing of user", bugs));
		}, function (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		});
};

userController.getUserProjects = function (req, res, next)
{
	if (!req.params.userid)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid user id'));
		return;
	}
	if (!utilsHelper.isValidObjectId(req.params.userid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid user id'));
		return;
	}

	var query = {};
	query.users = req.params.userid;

	projectsController._getProjects_(query)
		.then(
		function (projects)
		{
			res.status(200).send(apiResponseHelper.setResponse(true, "project listing of user", projects));
		}, function (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		});
};

userController._getUser_ = function (query)
{
	var deferred = $q.defer();

	User.findOne(query, function (err, user)
	{
		if (err)
		{
			deferred.reject(err);
		}
		else
		{
			if (user)
			{
				deferred.resolve(user);
			}
			else
			{
				deferred.reject(utilsHelper.newError(400, "No User found", "", 'nouserfound'));
			}
		}

	});

	return deferred.promise;
};

userController.getUsers = function (req, res, next)
{
	userController._getUsers_(req.params.workspaceid).then(function (users)
	{
		res.status(200).send(apiResponseHelper.setResponse(true, 'users list', users));
	}, function (err)
	{
		utilsHelper.mongooseErrorCallback(err);
	});

};

userController._getUsers_ = function (workspaceid,fields)
{
	var deferred = $q.defer();

	if(!fields)
	{
		fields = '_id name email role workspace_id'
	}
	User.find({'workspace_id': workspaceid},fields, function (err, users)
	{
		if (err)
		{
			deferred.reject(err);
		}
		else
		{
			deferred.resolve(users);
		}
	});

	return deferred.promise;
};

module.exports = userController;