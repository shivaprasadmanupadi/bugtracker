var portalController = {};
var workspaceController = require('../controllers/workspaceController');
var utilsHelper = require('../helpers/utilsHelper');
var apiResponseHelper = require('../helpers/apiResponseHelper');
var config = require('../config/config');

portalController.redirectToWorkspaceLogin = function (req, res, next)
{
	workspaceController._getWorkspace_({'name':req.params.workspacename}).then(function (workspace)
	{
		res.redirect('/portal/' + workspace._id + '/login');
	}, function (err)
	{
		utilsHelper.mongooseErrorCallback(err, res);
	});
};

portalController.renderLogin = function (req, res, next)
{
	if (!utilsHelper.isValidObjectId(req.params.workspaceid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid workspace id'));
		return;
	}

	workspaceController._getWorkspace_({'_id':req.params.workspaceid}).then(function (workspace)
	{
		res.locals.serverDetails = utilsHelper.getServerDetails(req);
		res.locals.workspace = workspace;
		res.render('portal/login');
	}, function (err)
	{
		utilsHelper.mongooseErrorCallback(err, res);
	});

};

portalController.signup = function (req, res, next)
{
	res.locals.serverDetails = utilsHelper.getServerDetails(req);
	res.render('portal/signup');
};

portalController.renderAdminConsole = function (req, res, next)
{
	res.locals.serverDetails = utilsHelper.getServerDetails(req);
	res.locals.user = req.decoded_token.user;
	res.locals.authToken = req.session.authToken;

	res.render('portal/workspace/admin');

};

portalController.renderDeveloperConsole = function (req, res, next)
{
	res.locals.serverDetails = utilsHelper.getServerDetails(req);
	res.locals.user = req.decoded_token.user;
	res.locals.authToken = req.session.authToken;

	res.render('portal/workspace/developer');

};

portalController.renderTriageConsole = function (req, res, next)
{
	res.locals.serverDetails = utilsHelper.getServerDetails(req);
	res.locals.user = req.decoded_token.user;
	res.locals.authToken = req.session.authToken;

	res.render('portal/workspace/triage');

};

portalController.logout = function (req, res, next)
{
	req.session.destroy();
	res.redirect('/portal/' + req.decoded_token.user.workspace_id + '/login');
};

module.exports = portalController;