var workspaceController = {};
var apiResponseHelper = require('../helpers/apiResponseHelper');

var config = require('../config/config');
var utilsHelper = require('../helpers/utilsHelper');
var Workspace = require('../models/workspace');
var $q = require('q');
var userController = require('../controllers/userController');

workspaceController.requestFilter = function (req, res, next)
{

};

workspaceController.createWorkspace = function (req, res, next)
{
    /*validating all fields because if second call validation fails then first DB transaction should be reverted*/

    if (!req.body.workspaceName)
    {
        res.status(400).send(apiResponseHelper.setResponse(false, "invalid organisation - please provide a valid workspace name"));
        return;
    }

    if (!req.body.name)
    {
        res.status(400).send(apiResponseHelper.setResponse(false, "invalid name - please provide a valid name"));
        return;
    }
    if (!req.body.email)
    {
        res.status(400).send(apiResponseHelper.setResponse(false, "invalid email - please provide a valid email id"));
        return;
    }
    if (!req.body.password)
    {
        res.status(400).send(apiResponseHelper.setResponse(false, "invalid password - please provide a valid password"));
        return;
    }

    workspaceController._createWorkspace_(req.body.workspaceName).then(function (workspace)
    {
        userController._createUser_(workspace._id, req.body.name, req.body.email, req.body.password, "admin").then(function (user)
        {
            res.status(200).send(apiResponseHelper.setResponse(true, "workspace created successfuly", workspace));
        }, function (err)
        {
            utilsHelper.mongooseErrorCallback(err, res);
        });
    }, function (err)
    {
        utilsHelper.mongooseErrorCallback(err, res);
    });
};

workspaceController._createWorkspace_ = function (name)
{
    var deferred = $q.defer();

    if (!name)
    {
        deferred.reject(utilsHelper.newError(400, "invalid name", "please provide a valid workspace name"));
        return deferred.promise;
    }

    new Workspace({'name': name}).save(function (err, workspace)
    {
        if (err)
        {
            deferred.reject(err);
        }
        else
        {
            deferred.resolve(workspace);
        }
    });
    return deferred.promise;
};

workspaceController._getWorkspace_ = function (query)
{
    var deferred = $q.defer();

    Workspace.findOne(query, function (err, workspace)
    {
        if (err)
        {
            deferred.reject(err);
        }
        else
        {
            if (workspace)
            {
                deferred.resolve(workspace);
            }
            else
            {
                deferred.reject(utilsHelper.newError(400, "workspace does not exists"));
            }
        }
    });
    return deferred.promise;
};

module.exports = workspaceController;