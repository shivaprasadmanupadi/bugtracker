var bugsController = {};
var apiResponseHelper = require('../helpers/apiResponseHelper');
var config = require('../config/config');
var Bugs = require('../models/bug');
var utilsHelper = require('../helpers/utilsHelper');
var $q = require('q');
var multer = require('multer');

/*create bug*/
bugsController.createBug = function (req, res, next)
{
	Bugs.count({'workspace_id':req.params.workspaceid,'project_id':req.params.projectid},function(error,count)
	{
		if(error)
		{

		}
		else
		{
			req.body.workspace_id = req.params.workspaceid;
			req.body.project_id = req.params.projectid;
			req.body.created_by = req.decoded_token.user._id;
			req.body.should_be_verified_by = req.decoded_token.user._id;
			req.body.activity_log = ["created by " + req.body.created_by + " on " + new Date()];
			req.body.identifier = req.project.name.substring(0,8) + (count+1);

			bugsController._createBug_(req.body).then(function (bug)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'bug created', bug));
			}, function (err)
			{
				utilsHelper.mongooseErrorCallback(err, res);
			});
		}
	});
};

bugsController._createBug_ = function (bug)
{
	var deferred = $q.defer();

	new Bugs(bug).save(function (err, bug)
	{
		if (err)
		{
			deferred.reject(err);
		}
		else
		{
			deferred.resolve(bug);
		}
	});

	return deferred.promise;
};
/*end of create bug*/

/*get project bugs*/
bugsController.getProjectBugs = function (req, res, next)
{
	var query = {};
	var fields = '_id title identifier project_id workflow_queue status severity current_user created_by';
	var options = {};
	options.sort = '-created_at';

	query.project_id = req.params.projectid;

	options.limit = 5;

	if(!req.query.page)
	{
		req.query.page = 0;
	}
	else
	{
		req.query.page -= 1;
	}

	options.skip = req.query.page * options.limit;

	if(req.query.status)
	{
		query.status = req.query.status;
	}

    if(req.query.severity)
    {
        query.severity = req.query.severity;
    }

	if(req.query.workflow)
	{
        query.workflow_queue = req.query.workflow;
	}

	if(req.query.userid)
	{
		query.current_user = req.query.userid;
	}

    if(req.query.created_by)
    {
        query.created_by = req.query.created_by;
    }

	if(req.query.category)
	{
		query.category = req.query.category;
	}

	Bugs.count(query,function(err,count)
	{
		bugsController._getProjectBugs_(query,fields,options)
			.then(
			function (bugs)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, "bugs listing of project", bugs,{},count));
			}, function (err)
			{
				utilsHelper.mongooseErrorCallback(err, res);
			});
	});
};

bugsController._getProjectBugs_ = function (query,fields,options)
{
	var deferred = $q.defer();
    if(!fields)
	{
		fields = '_id title identifier project_id workflow_queue status severity current_user created_by';
	}
	if(!options)
	{
		options = {};
	}
	Bugs.find(query,fields,options).populate('created_by current_user','name').exec(function (err, bugs)
	{
		if (err)
		{
			deferred.reject(err);
		}
		else
		{
			deferred.resolve(bugs);
		}
	});

	return deferred.promise;
};
/*end of get project bugs*/

/*get bug details*/
bugsController.getBug = function (req, res, next)
{
	res.status(200).send(apiResponseHelper.setResponse(true, "bugs details", req.bug));
};

bugsController._getBug_ = function (query)
{
	var deferred = $q.defer();

	Bugs.findOne(query).populate('created_by current_user','name').exec(function (err, bug)
	{
		if (err)
		{
			deferred.reject(err);
		}
		else
		{
			if (bug)
			{
				deferred.resolve(bug);
			}
			else
			{
				deferred.reject(utilsHelper.newError(400, 'bug not found'));
			}
		}
	});

	return deferred.promise;
};
/*end of get bug details*/

/*delete bug*/
bugsController.deleteBug = function (req, res, next)
{
	req.bug.remove(function (err, removedLength)
	{
		if (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		}
		else
		{
			if (removedLength)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'bug deleted'));
			}
			else
			{
				res.status(200).send(apiResponseHelper.setResponse(false, 'bug not deleted'));
			}
		}
	});
};

/*end of delete bug*/

/*add comment*/
bugsController.addComment = function (req, res, next)
{
	if (!req.body.comment)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid comment'));
		return;
	}

	req.comment = {};
	req.comment.by = req.decoded_token.user.name;
	req.comment.on = new Date();
	req.comment.type = "Generic Comment";
	req.comment.comment = req.body.comment;

	req.bug.update({$set: {updated_at: new Date(), updated_by: req.decoded_token.user.name},
		$push           : {
			comments    : req.comment,
			activity_log: 'comment added by  ' + req.decoded_token.user.name + ' on ' + new Date()
		}
	}, function (err, updatedLength)
	{
		if (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		}
		else
		{
			if (updatedLength)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'comment added', req.comment));
			}
			else
			{
				res.status(200).send(apiResponseHelper.setResponse(false, 'bug not found'));
			}
		}
	});
};
/*end of add comment*/

/*upload screenshot*/

bugsController.uploadScreenshot = function (req, res, next)
{

	var storage = multer.diskStorage({
		destination: './application/uploads/screenshots',
		filename   : function (req, file, cb)
		{
			cb(null, req.bug._id + "_" + Date.now() + '.png')
		}
	});

	var multerUpload = multer({
		storage   : storage,
		limits    : {
			fileSize: 0.5 * 1000 * 1000
		},
		fileFilter: function (req, file, cb)
		{
			var getFileExt = function (fileName)
			{
				var fileExt = fileName.split(".");
				if (fileExt.length === 1 || ( fileExt[0] === "" && fileExt.length === 2 ))
				{
					return "";
				}
				return fileExt.pop();
			};

			var extension = getFileExt(file.originalname);
			if (extension == 'jpg' || extension == 'png' || extension == 'JPG' || extension == 'PNG')
			{
				cb(null, true);
			}
			else
			{
				cb(new Error('invalid extension.'));
			}
		}
	});
	var uploadFile = multerUpload.single('screenshot');

	uploadFile(req, res, function (err)
	{
		if (err)
		{
			res.status(400).send(apiResponseHelper.setResponse(false, 'upload failed', null, err));
		}
		else
		{
			req.screenshot = {};
			req.screenshot.by = req.decoded_token.user.name;
			req.screenshot.by_id = req.decoded_token.user._id;
			req.screenshot._id = utilsHelper.generateObjectId();
			req.screenshot.filename = req.file.filename;
			req.screenshot.on = new Date();
			req.bug.update({$set: {updated_at: new Date(), updated_by: req.decoded_token.user.name},
				$push           : {
					screenshots : req.screenshot,
					activity_log: 'screenshot added by  ' + req.decoded_token.user.name + ' on ' + new Date()
				}
			}, function (err, updatedlength)
			{
				if (err)
				{
					utilsHelper.mongooseErrorCallback(err, res);
				}
				else
				{
					res.status(200).send(apiResponseHelper.setResponse(true, 'image details', req.screenshot));
				}
			});
		}
	});
};

/*end of upload screenshot*/

/*upload attachment*/

bugsController.uploadAttachment = function (req, res, next)
{

	var storage = multer.diskStorage({
		destination: './application/uploads/attachments',
		filename   : function (req, file, cb)
		{
			cb(null, req.bug._id + "_" + Date.now() + "_" + file.originalname)
		}
	});

	var multerUpload = multer({
		storage   : storage,
		limits    : {
			fileSize: 2 * 1000 * 1000
		}
	});
	var uploadFile = multerUpload.single('attachment');

	uploadFile(req, res, function (err)
	{
		if (err)
		{
			res.status(400).send(apiResponseHelper.setResponse(false, 'upload failed', null, err));
		}
		else
		{
			req.attachment = {};
			req.attachment.by = req.decoded_token.user.name;
			req.attachment.filename = req.file.filename;
			req.attachment.on = new Date();
			req.bug.update({$set: {updated_at: new Date(), updated_by: req.decoded_token.user.name},
				$push           : {
					attachments : req.attachment,
					activity_log: 'attachment added by  ' + req.decoded_token.user.name + ' on ' + new Date()
				}
			}, function (err, updatedlength)
			{
				if (err)
				{
					utilsHelper.mongooseErrorCallback(err, res);
				}
				else
				{
					res.status(200).send(apiResponseHelper.setResponse(true, 'image details', req.attachment));
				}
			});
		}
	});
};

/*end of upload attachment*/


/*assign user*/
bugsController.assignUser = function (req, res, next)
{
	if (!utilsHelper.isValidObjectId(req.params.bugid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid bug id'));
		return;
	}

	if (!utilsHelper.isValidObjectId(req.params.userid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid user id'));
		return;
	}

	req.bug.update({$set: {updated_at: new Date(), updated_by: req.decoded_token.user.name,current_user:req.params.userid,perform_user:req.params.userid,workflow_queue:'perform'},
		$push           : {
			activity_log: 'bug assigned by  ' + req.decoded_token.user.name + ' on ' + new Date()
		}
	}, function (err, updatedLength)
	{
		if (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		}
		else
		{
			if (updatedLength)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'bug assigned'));
			}
			else
			{
				res.status(200).send(apiResponseHelper.setResponse(false, 'bug not found'));
			}
		}
	});
};
/*end of assign user*/

/*update workflow*/
bugsController.updateWorkflow = function (req, res, next)
{
	if(!req.body.should_be_verified_by)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid user to verify the bug'));
		return;
	}


	if (!utilsHelper.isValidObjectId(req.body.should_be_verified_by))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid user to verify the bug'));
		return;
	}

	if(!req.body.perform_user)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid perform user'));
		return;
	}

	if (!utilsHelper.isValidObjectId(req.body.perform_user))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid perform user'));
		return;
	}


	var query = {};
	query.$set = {};
	query.$push = {};
	query.$set.updated_at = new Date();
	query.$set.updated_by = req.decoded_token.user.name;
	query.$set.perform_user = req.body.perform_user;
	query.$set.should_be_verified_by = req.body.should_be_verified_by;

	if(req.bug.workflow_queue == 'perform')
	{
		query.$set.current_user = req.body.perform_user;
		query.$push.activity_log = 'workflow updated by  ' + req.decoded_token.user.name + ' on ' + new Date() + '. perform user from ' + req.bug.perform_user + ' to ' + req.body.perform_user + '. verify and close user from ' + req.bug.should_be_verified_by + ' to ' + req.body.should_be_verified_by + '. bug is now assigned to ' + req.body.perform_user + ' by ' + req.decoded_token.user.name;
	}
	else if(req.bug.workflow_queue == 'verifyandclose')
	{
		query.$set.current_user = req.body.should_be_verified_by;
		query.$push.activity_log = 'workflow updated by  ' + req.decoded_token.user.name + ' on ' + new Date() + '. perform user from ' + req.bug.perform_user + ' to ' + req.body.perform_user + '. verify and close user from ' + req.bug.should_be_verified_by + ' to ' + req.body.should_be_verified_by + '. bug is now assigned to ' + req.body.should_be_verified_by + ' by ' + req.decoded_token.user.name;
	}

	req.bug.update(query, function (err, updatedLength)
	{
		if (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		}
		else
		{
			if (updatedLength)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'bug assigned'));
			}
			else
			{
				res.status(200).send(apiResponseHelper.setResponse(false, 'bug not found'));
			}
		}
	});
};
/*end of update workflow*/

/*reject*/
bugsController.reject = function (req, res, next)
{
	var query = {};
	query.$set = {};
	query.$push = {};
	query.$set.updated_at = new Date();
	query.$set.updated_by = req.decoded_token.user.name;
	query.$push.activity_log = 'rejected by ' + req.decoded_token.user.name + ' on ' + new Date();
	if(req.body.rejectionMessage)
	{
		req.comment = {};
		req.comment.by = req.decoded_token.user.name;
		req.comment.on = new Date();
		req.comment.type = "Rejection Comment";
		req.comment.comment = req.body.rejectionMessage;
		query.$push.comments = req.comment;
	}
	if(req.bug.workflow_queue == 'perform')
	{
		query.$unset = {};
		query.$set.workflow_queue = 'triage';
		query.$unset.current_user = 1;
		query.$unset.perform_user = 1;
	}
	else if (req.bug.workflow_queue == 'verifyandclose')
	{
		query.$set.workflow_queue = 'perform';
		query.$set.current_user = req.bug.perform_user;
	}
	req.bug.update(query, function (err, updatedLength)
	{
		if (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		}
		else
		{
			if (updatedLength)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'bug assigned'));
			}
			else
			{
				res.status(200).send(apiResponseHelper.setResponse(false, 'bug not found'));
			}
		}
	});
};
/*end of reject*/

/*route*/
bugsController.route = function (req, res, next)
{
	if(!req.body.resolution_status)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid resolution status'));
		return;
	}

	var query = {};
	query.$set = {};
	query.$push = {};
	query.$set.updated_at = new Date();
	query.$set.resolution_status = req.body.resolution_status;
	query.$set.current_user = req.bug.should_be_verified_by;
	query.$set.updated_by = req.decoded_token.user.name;
	query.$set.workflow_queue = "verifyandclose";
	query.$push.activity_log = 'routed by ' + req.decoded_token.user.name + ' on ' + new Date() + ". with resolution status as " + req.body.resolution_status;
	if(req.body.resolution_message)
	{
		req.comment = {};
		req.comment.by = req.decoded_token.user.name;
		req.comment.on = new Date();
		req.comment.type = "Routing Comment";
		req.comment.comment = req.body.resolution_message;
		query.$push.comments = req.comment;
	}
	if(req.body.fixed_in_build)
	{
		query.$set.fixed_in_build = req.body.fixed_in_build;
	}
	req.bug.update(query, function (err, updatedLength)
	{
		if (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		}
		else
		{
			if (updatedLength)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'bug updated'));
			}
			else
			{
				res.status(200).send(apiResponseHelper.setResponse(false, 'bug not found'));
			}
		}
	});
};
/*route*/


/*verified*/
bugsController.verified = function (req, res, next)
{
	if(!req.body.verificationMessage)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid verification message'));
		return;
	}

	var query = {};
	query.$set = {};
	query.$push = {};
	query.$set.updated_at = new Date();
	query.$set.updated_by = req.decoded_token.user.name;
	query.$set.workflow_queue = "closed";
	query.$set.status = 'closed';
	query.$set.verified_by = req.decoded_token.user._id;
	query.$push.activity_log = 'closed by ' + req.decoded_token.user.name + ' on ' + new Date() + ". with verification message as " + req.body.verificationMessage;
	query.$unset = {};
	query.$unset.current_user = 1;
	if(req.body.verificationMessage)
	{
		req.comment = {};
		req.comment.by = req.decoded_token.user.name;
		req.comment.on = new Date();
		req.comment.type = "Verification Comment";
		req.comment.comment = req.body.verificationMessage;
		query.$push.comments = req.comment;
	}
	if(req.body.fixed_in_build)
	{
		query.$set.fixed_in_build = req.body.fixed_in_build;
	}
	req.bug.update(query, function (err, updatedLength)
	{
		if (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		}
		else
		{
			if (updatedLength)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'bug updated'));
			}
			else
			{
				res.status(200).send(apiResponseHelper.setResponse(false, 'bug not found'));
			}
		}
	});
};
/*verified*/


/*reopen*/
bugsController.reopen = function (req, res, next)
{
	var query = {};
	query.$set = {};
	query.$push = {};
	query.$set.updated_at = new Date();
	query.$set.updated_by = req.decoded_token.user.name;
	query.$set.workflow_queue = "triage";
	query.$set.status = 'open';
	query.$push.activity_log = 're-opened by ' + req.decoded_token.user.name + ' on ' + new Date() + ".";
	if(req.body.reopenMessage)
	{
		req.comment = {};
		req.comment.by = req.decoded_token.user.name;
		req.comment.on = new Date();
		req.comment.type = "Generic Comment";
		req.comment.comment = req.body.reopenMessage;
		query.$push.comments = req.comment;
	}
	req.bug.update(query, function (err, updatedLength)
	{
		if (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		}
		else
		{
			if (updatedLength)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'bug updated'));
			}
			else
			{
				res.status(200).send(apiResponseHelper.setResponse(false, 'bug not found'));
			}
		}
	});
};
/*reopen*/

/*edit*/
bugsController.edit = function (req, res, next)
{
	if(!req.body.title)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid title'));
		return;
	}

	if(!req.body.description)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid description'));
		return;
	}

	if(!req.body.severity)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid severity'));
		return;
	}

	var query = {};
	query.$set = {};
	query.$push = {};
	query.$set.updated_at = new Date();
	query.$set.updated_by = req.decoded_token.user.name;
	query.$set.title = req.body.title;
	query.$set.description = req.body.description;
	query.$set.severity = req.body.severity;
	query.$set.category = req.body.category;
	query.$set.target_release = req.body.target_release;
	query.$set.found_in_build= req.body.found_in_build;
	query.$set.steps_to_reproduce= req.body.steps_to_reproduce;
	query.$set.regression = req.body.regression;
	query.$push.activity_log = 'bug details updated by ' + req.decoded_token.user.name + ' on ' + new Date() + ".";

	req.bug.update(query, function (err, updatedLength)
	{
		if (err)
		{
			utilsHelper.mongooseErrorCallback(err, res);
		}
		else
		{
			if (updatedLength)
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'bug updated'));
			}
			else
			{
				res.status(200).send(apiResponseHelper.setResponse(false, 'bug not found'));
			}
		}
	});
};
/*edit*/

/*delete screenshot*/
bugsController.deleteScreenshot = function (req, res, next)
{
	if (!utilsHelper.isValidObjectId(req.params.screenshotid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid screenshot id'));
		return;
	}

	req.bug.updated_at = new Date();
	req.bug.updated_by = req.decoded_token.user.name;
	req.bug.activity_log.push('screenshot deleted by  ' + req.decoded_token.user.name + ' on ' + new Date());
	for(var index=0;index<req.bug.screenshots.length;index++)
	{
		if(req.bug.screenshots[index]._id == req.params.screenshotid)
		{
			req.bug.screenshots.splice(index,1);
			req.screenshotfound = true;
			break;
		}
	}
	req.bug.markModified('screenshots');
	if(req.screenshotfound)
	{
		req.bug.save(function(err,bug)
		{
			if (err)
			{
				utilsHelper.mongooseErrorCallback(err, res);
			}
			else
			{
				res.status(200).send(apiResponseHelper.setResponse(true, 'screenshot deleted'));
			}
		});
	}
	else
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid screenshot id'));
		return;
	}
};
/*end of delete screenshot*/

bugsController._updateBug_ = function (query, updateQuery)
{
	var deferred = $q.defer();

	Bugs.update(query, updateQuery, function (err, updatedCount)
	{
		if (err)
		{
			deferred.reject(err);
		}
		else
		{
			deferred.resolve(updatedCount);
		}
	});

	return deferred.promise;
};

module.exports = bugsController;