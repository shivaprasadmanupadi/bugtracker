var apiResponseHelper = {};

apiResponseHelper.setResponse = function (flag, message, payload, errorPayload,meta)
{
	apiResponse =
	{};
	apiResponse.result =
	{};
	apiResponse.payload =
	{};

	apiResponse.result.success = flag;
	apiResponse.result.message = message;
	apiResponse.result.error = errorPayload;

	apiResponse.payload = payload;
	apiResponse.meta = meta;

	return apiResponse;
};

module.exports = apiResponseHelper;