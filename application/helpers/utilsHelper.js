var utilsHelper = {};
var nodeUuid = require('node-uuid');
var bcrypt = require('bcrypt-nodejs');
var apiResponseHelper = require('./apiResponseHelper');
var crypto = require('crypto');
var config = require('../config/config');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var $q = require('q');

utilsHelper.generateRandomKey = function (isTimeBased)
{
	if (isTimeBased)
	{
		return nodeUuid.v1();
	}

	return nodeUuid.v4();
};

utilsHelper.generateObjectId = function()
{
	return mongoose.Types.ObjectId();
};

utilsHelper.extractMongooseError = function (errorslistobject)
{
	var message = "";
	for (key in errorslistobject)
	{
		message = errorslistobject[key].name + "-" + errorslistobject[key].message;
	}
	return message;
};

utilsHelper.generateHash = function (text)
{
	return bcrypt.hashSync(text, bcrypt.genSaltSync(8), null);
};

utilsHelper.mongooseErrorCallback = function (err, res)
{
	if (err.errors)
	{
		res.status(500).send(apiResponseHelper.setResponse(false, utilsHelper.extractMongooseError(err.errors), null, err.errors));
	}
	else
	{
		res.status(err.status ? err.status : 500).send(apiResponseHelper.setResponse(false, err.message ? err.name + " - " + err.message : err.name, null));
	}
};

utilsHelper.newError = function (status, name, message, flag)
{
	var err = {};

	err.status = status;
	err.name = name;
	err.message = message;
	err.flag = flag;

	return err;
};

utilsHelper.encrypt = function (text)
{
	var cipher = crypto.createCipher(config.encryption.algorithm, config.encryption.secret);
	var crypted = cipher.update(text, 'utf8', 'hex')
	crypted += cipher.final('hex');
	return crypted;
};

utilsHelper.decrypt = function (text)
{
	try
	{
		var decipher = crypto.createDecipher(config.encryption.algorithm, config.encryption.secret);
		var dec = decipher.update(text, 'hex', 'utf8')
		dec += decipher.final('utf8');
		return dec;
	}
	catch (error)
	{
		return 0;
	}
};

utilsHelper.isValidObjectId = function (str)
{
	return mongoose.Types.ObjectId.isValid(str);
};

utilsHelper.stringToObjectId = function (str)
{
	return mongoose.Types.ObjectId(str);
};

utilsHelper.getServerDetails = function (req)
{
	var serverDetails =
	{};
	serverDetails.contextPath = req.protocol + '://' + req.headers.host;
	serverDetails.api = serverDetails.contextPath + '/' + config.constants.apiPath + '/' + config.constants.apiVersion;
	serverDetails.appName = config.constants.appName;
	serverDetails.appVersion = config.constants.appVersion;
	return serverDetails;
};

utilsHelper.validateWorkspaceToken = function (workspaceid, token)
{
	var token = utilsHelper.decrypt(token);
	var deferred = $q.defer();

	if (!token)
	{
		deferred.reject(utilsHelper.newError(400, "invalid token"));
		return deferred.promise;
	}

	jwt.verify(token, config.jwt.secret, function (err, decoded)
	{
		if (err)
		{
			deferred.reject(err);
			return deferred.promise;
		}
		else
		{
			if (workspaceid == decoded.user.workspace_id)
			{
				deferred.resolve(decoded);
				return deferred.promise;
			}
			else
			{
				deferred.reject(utilsHelper.newError(400, "unauthorized workspace access token"));
				return deferred.promise;
			}
		}
	});

	return deferred.promise;
};

module.exports = utilsHelper;