var emailService = {};

var nodemailer = require('nodemailer');
var config = require('../config/config');

var transporter = nodemailer.createTransport(config.emailSettings.url);

emailService.sendMail = function(from,to,subject,text,html)
{
    if(from == 'bot')
    {
        from = config.emailSettings.url;
    }

    var mailOptions = {
        from: from,
        to: to,
        subject: subject,
        text: text,
        html: html
    };

    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
};

module.exports = emailService;