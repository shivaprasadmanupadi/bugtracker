var apiRouter = require('express').Router({mergeParams: true});

var authController = require('../controllers/authController');
var workspaceController = require('../controllers/workspaceController');
var projectsController = require('../controllers/projectsController');
var userController = require('../controllers/userController');
var bugsController = require('../controllers/bugsController');
var requestFilter = require('../filters/requestFilter');

apiRouter.post('/workspace', workspaceController.createWorkspace);
apiRouter.post('/workspace/auth/login', authController.login);

apiRouter.use('/workspace/:workspaceid', requestFilter.accessWorkspace);

apiRouter.post('/workspace/:workspaceid/projects', requestFilter.isAdmin, projectsController.createProject);
apiRouter.get('/workspace/:workspaceid/projects', requestFilter.isAdmin, projectsController.getProjects);
apiRouter.get('/workspace/:workspaceid/projects/:projectid', requestFilter.accessProject, projectsController.getProject);

apiRouter.post('/workspace/:workspaceid/projects/:projectid/settings/categories', requestFilter.accessProject, projectsController.addCategory);
apiRouter.post('/workspace/:workspaceid/projects/:projectid/settings/releases', requestFilter.accessProject, projectsController.addRelease);
apiRouter.post('/workspace/:workspaceid/projects/:projectid/settings/configuration', requestFilter.accessProject, projectsController.updateConfiguration);

apiRouter.post('/workspace/:workspaceid/projects/:projectid/users/:userid', requestFilter.isAdmin, requestFilter.accessProject, projectsController.linkUser);
apiRouter.get('/workspace/:workspaceid/projects/:projectid/users', requestFilter.accessProject, projectsController.getProjectUsers);
apiRouter.delete('/workspace/:workspaceid/projects/:projectid/users/:userid', requestFilter.isAdmin, requestFilter.accessProject, projectsController.unlinkUser);

apiRouter.post('/workspace/:workspaceid/users', requestFilter.isAdmin, userController.createUser);
apiRouter.get('/workspace/:workspaceid/users', requestFilter.isAdmin, userController.getUsers);
apiRouter.get('/workspace/:workspaceid/users/:userid', requestFilter.isAdmin, userController.getUser);
apiRouter.get('/workspace/:workspaceid/users/:userid/bugs', userController.getUserBugs);
apiRouter.get('/workspace/:workspaceid/users/:userid/projects', userController.getUserProjects);

apiRouter.post('/workspace/:workspaceid/projects/:projectid/bugs', requestFilter.accessProject, bugsController.createBug);
apiRouter.get('/workspace/:workspaceid/projects/:projectid/bugs', requestFilter.accessProject, bugsController.getProjectBugs);
apiRouter.get('/workspace/:workspaceid/projects/:projectid/bugs/:bugid', requestFilter.accessBug, bugsController.getBug);
apiRouter.delete('/workspace/:workspaceid/projects/:projectid/bugs/:bugid', requestFilter.isAdmin, requestFilter.accessBug, bugsController.deleteBug);

apiRouter.post('/workspace/:workspaceid/projects/:projectid/bugs/:bugid/comments', requestFilter.accessBug, bugsController.addComment);
apiRouter.post('/workspace/:workspaceid/projects/:projectid/bugs/:bugid/assign/:userid',requestFilter.isTriage, requestFilter.accessBug, bugsController.assignUser);
apiRouter.post('/workspace/:workspaceid/projects/:projectid/bugs/:bugid/screenshots', requestFilter.accessBug, bugsController.uploadScreenshot);
apiRouter.delete('/workspace/:workspaceid/projects/:projectid/bugs/:bugid/screenshots/:screenshotid', requestFilter.accessBug, bugsController.deleteScreenshot);
apiRouter.post('/workspace/:workspaceid/projects/:projectid/bugs/:bugid/attachments', requestFilter.accessBug, bugsController.uploadAttachment);
apiRouter.post('/workspace/:workspaceid/projects/:projectid/bugs/:bugid/workflow', requestFilter.accessBug, bugsController.updateWorkflow);
apiRouter.post('/workspace/:workspaceid/projects/:projectid/bugs/:bugid/reject', requestFilter.accessBug, bugsController.reject);
apiRouter.post('/workspace/:workspaceid/projects/:projectid/bugs/:bugid/route', requestFilter.accessBug, bugsController.route);
apiRouter.post('/workspace/:workspaceid/projects/:projectid/bugs/:bugid/verified', requestFilter.accessBug, bugsController.verified);
apiRouter.post('/workspace/:workspaceid/projects/:projectid/bugs/:bugid/reopen', requestFilter.accessBug, bugsController.reopen);
apiRouter.put('/workspace/:workspaceid/projects/:projectid/bugs/:bugid', requestFilter.accessBug, bugsController.edit);

module.exports = apiRouter;