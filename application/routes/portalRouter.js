var portalRouter = require('express').Router({mergeParams: true});
var portalController = require('../controllers/portalController');
var requestFilter = require('../filters/requestFilter');

portalRouter.get('/signup', portalController.signup);

portalRouter.get('/portal/:workspacename', portalController.redirectToWorkspaceLogin);
portalRouter.get('/portal/:workspaceid/login', portalController.renderLogin);

portalRouter.get('/portal/:workspaceid/logout', requestFilter.accessWorkspaceBySession, portalController.logout);
portalRouter.get('/portal/:workspaceid/admin/console', requestFilter.accessWorkspaceBySession, portalController.renderAdminConsole);
portalRouter.get('/portal/:workspaceid/developer/console', requestFilter.accessWorkspaceBySession, portalController.renderDeveloperConsole);
portalRouter.get('/portal/:workspaceid/triage/console', requestFilter.accessWorkspaceBySession, portalController.renderTriageConsole);

module.exports = portalRouter;