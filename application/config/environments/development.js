var config = {};

config.database = {};
config.database.type = 'mongodb';
config.database.dbname = 'bugtracker';
config.database.username = 'admin';
config.database.password = 'admin';
config.database.host = "localhost";
config.database.port = "27017";
config.database.options =
{
    server: {
        socketOptions: {
            keepAlive: 1
        }
    }
};
config.database.url
    = config.database.type + "://" + config.database.username + ":" + config.database.password + "@" + config.database.host + ":" + config.database.port + "/" + config.database.dbname;

config.emailSettings = {};
config.emailSettings.email = '';
config.emailSettings.password = '';
config.emailSettings.url = 'smtps://' + config.emailSettings.email + ':' + config.emailSettings.password + '@smtp.gmail.com';
module.exports = config;