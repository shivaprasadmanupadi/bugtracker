var requestFilter = {};
var jwt = require('jsonwebtoken');
var apiResponseHelper = require('../helpers/apiResponseHelper');
var config = require('../config/config');
var utilsHelper = require('../helpers/utilsHelper');
var projectsController = require('../controllers/projectsController');
var bugsController = require('../controllers/bugsController');

requestFilter.accessWorkspace = function (req, res, next)
{
	if (!utilsHelper.isValidObjectId(req.params.workspaceid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, "invalid workspace id " + req.params.workspaceid));
		return;
	}

	utilsHelper.validateWorkspaceToken(req.params.workspaceid, req.headers.authorization).then(function (decoded)
	{
		req.decoded_token = decoded;
		next();
	}, function (err)
	{
		utilsHelper.mongooseErrorCallback(err, res);
	});

};

requestFilter.accessWorkspaceBySession = function (req, res, next)
{
	if (!utilsHelper.isValidObjectId(req.params.workspaceid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, "invalid workspace id " + req.params.workspaceid));
		return;
	}

	utilsHelper.validateWorkspaceToken(req.params.workspaceid, req.session.authToken).then(function (decoded)
	{
		req.decoded_token = decoded;
		next();
	}, function (err)
	{
		utilsHelper.mongooseErrorCallback(err, res);
	});

};

requestFilter.isAdmin = function (req, res, next)
{
	if (req.decoded_token.user.role == 'admin')
	{
		next();
	}
	else
	{
		res.status(401).send(apiResponseHelper.setResponse(false, "unauthorized resource access for role " + req.decoded_token.user.role));
	}
};

requestFilter.isTriage = function (req, res, next)
{
	if (req.decoded_token.user.role == 'admin' || req.decoded_token.user.role == 'triage')
	{
		next();
	}
	else
	{
		res.status(401).send(apiResponseHelper.setResponse(false, "unauthorized resource access for role " + req.decoded_token.user.role));
	}
};

requestFilter.accessProject = function (req, res, next)
{
	if (!req.params.projectid)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid project id'));
		return;
	}

	if (!utilsHelper.isValidObjectId(req.params.projectid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, "invalid project id " + req.params.projectid));
		return;
	}

	projectsController._getProject_({
		'_id'         : req.params.projectid,
		'workspace_id': req.params.workspaceid
	}).then(function (project)
	{
		req.project = project;
		next();
	}, function (err)
	{
		utilsHelper.mongooseErrorCallback(err, res);
	});
};

requestFilter.accessBug = function (req, res, next)
{
	if (!req.params.projectid)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid project id'));
		return;
	}

	if (!utilsHelper.isValidObjectId(req.params.projectid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, "invalid project id " + req.params.projectid));
		return;
	}

	if (!req.params.bugid)
	{
		res.status(400).send(apiResponseHelper.setResponse(false, 'invalid bug id'));
		return;
	}

	if (!utilsHelper.isValidObjectId(req.params.bugid))
	{
		res.status(400).send(apiResponseHelper.setResponse(false, "invalid bug id " + req.params.bugid));
		return;
	}

	bugsController._getBug_({
		'_id'         : req.params.bugid,
		'workspace_id': req.params.workspaceid,
		'project_id'  : req.params.projectid
	}).then(function (bug)
	{
		req.bug = bug;
		next();
	}, function (err)
	{
		utilsHelper.mongooseErrorCallback(err, res);
	});
};

module.exports = requestFilter;