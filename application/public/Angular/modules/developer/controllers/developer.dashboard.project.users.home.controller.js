developer.controller('developerDashboardProjectUsersHomeController', ['$scope', '$http', '$rootScope', 'utils', '$stateParams', 'portalServices', function ($scope, $http, $rootScope, utils, $stateParams, portalServices)
{
    $scope.unassociatedUsers = [];

    $scope.getUsers = function ()
    {
        workspaceUsers.then(function (users)
        {
            $scope.unassociatedUsers = utils.arraysDiff(users, $scope.projectUsers);
            $('#associateUserModal').modal('show');
        });
    };

}]);