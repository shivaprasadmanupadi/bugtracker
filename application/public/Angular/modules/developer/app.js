var developer = angular.module('developer', ['ui.router', 'helpers']);

developer.factory('requestInterceptor', ['$rootScope', function ($rootScope)
{
    var requestInterceptor = {};

    requestInterceptor.request = function (config)
    {
        config.headers['Authorization'] = $rootScope.authToken;
        return config;
    };

    return requestInterceptor;
}]);

developer.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider)
{
    $httpProvider.interceptors.push('requestInterceptor');

    $stateProvider.state('developer',
        {
            abstract: true,
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.html'
        }).
        state('developer.dashboard',
        {
            abstract: true,
            url: 'dashboard',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.html'
        }).
        state('developer.dashboard.home',
        {
            url: '/home',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.home.html',
            controller: 'developerDashboardHomeController'
        }).
        state('developer.dashboard.mywork',
        {
            url: '/mywork',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.mywork.html',
            controller: 'developerDashboardMyworkController'
        }).
        state('developer.dashboard.project',
        {
            url: '/project/:projectid',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.project.html',
            controller: 'developerDashboardProjectController',
            /*
             resolve:{
             currentProject:function($stateParams,$q,$http,$rootScope,utils)
             {
             var deferred = $q.defer();

             $http(
             {
             method : 'GET' ,
             url : $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $stateParams.projectid
             }).success(function(data , status , headers , config)
             {
             if (data.result.success)
             {
             deferred.resolve(data.payload);
             }
             else
             {
             utils.showError(data.result.message);
             deferred.reject(data.payload);
             }
             }).error(function(data , status , headers , config)
             {
             utils.showError(data.result.message);
             deferred.reject(data.payload);
             }).finally(function()
             {
             });

             return deferred.promise;
             }
             }*/
        }).
        state('developer.dashboard.project.bugs',
        {
            url: '/bugs',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.project.bugs.html',
            redirectTo: 'developer.dashboard.project.bugs.home'
        }).
        state('developer.dashboard.project.bugs.home',
        {
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.project.bugs.home.html',
            controller: 'developerDashboardProjectBugsHomeController'
        }).
        state('developer.dashboard.project.bugs.create',
        {
            url: '/create',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.project.bugs.create.html',
            controller: 'developerDashboardProjectBugsCreateController'
        }).
        state('developer.dashboard.project.bugs.edit',
        {
            url: '/edit/:bugid',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.project.bugs.edit.html',
            controller: 'developerDashboardProjectBugsEditController'
        }).
        state('developer.dashboard.project.users',
        {
            url: '/users',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.project.users.html',
            redirectTo: 'developer.dashboard.project.users.home'
        }).state('developer.dashboard.project.users.home',
        {
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.project.users.home.html',
            controller: 'developerDashboardProjectUsersHomeController'
        }).
        state('developer.dashboard.project.closed',
        {
            url: '/closed',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/developer/templates/developer.dashboard.project.closed.html',
            controller: 'developerDashboardProjectClosedController'
        });
    $urlRouterProvider.otherwise('/dashboard/mywork');
}]);

developer.run(['$rootScope', 'utils', '$state', function ($rootScope, utils, $state)
{
    $rootScope.serverDetails = serverDetails;
    $rootScope.authToken = authToken;
    $rootScope.user = user;

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState)
    {
        if (toState.redirectTo)
        {
            event.preventDefault();
            $state.go(toState.redirectTo, toParams);
        }
    });

}]);