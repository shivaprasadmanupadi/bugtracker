var triage = angular.module('triage', ['ui.router', 'helpers']);

triage.factory('requestInterceptor', ['$rootScope', function ($rootScope)
{
    var requestInterceptor = {};

    requestInterceptor.request = function (config)
    {
        config.headers['Authorization'] = $rootScope.authToken;
        return config;
    };

    return requestInterceptor;
}]);

triage.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider)
{
    $httpProvider.interceptors.push('requestInterceptor');

    $stateProvider.state('triage',
        {
            abstract: true,
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.html'
        }).
        state('triage.dashboard',
        {
            abstract: true,
            url: 'dashboard',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.html'
        }).
        state('triage.dashboard.home',
        {
            url: '/home',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.home.html',
            controller: 'triageDashboardHomeController'
        }).
        state('triage.dashboard.mywork',
        {
            url: '/mywork',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.mywork.html',
            controller: 'triageDashboardMyworkController'
        }).
        state('triage.dashboard.project',
        {
            url: '/project/:projectid',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.project.html',
            controller: 'triageDashboardProjectController',
            /*
             resolve:{
             currentProject:function($stateParams,$q,$http,$rootScope,utils)
             {
             var deferred = $q.defer();

             $http(
             {
             method : 'GET' ,
             url : $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $stateParams.projectid
             }).success(function(data , status , headers , config)
             {
             if (data.result.success)
             {
             deferred.resolve(data.payload);
             }
             else
             {
             utils.showError(data.result.message);
             deferred.reject(data.payload);
             }
             }).error(function(data , status , headers , config)
             {
             utils.showError(data.result.message);
             deferred.reject(data.payload);
             }).finally(function()
             {
             });

             return deferred.promise;
             }
             }*/
        }).
        state('triage.dashboard.project.bugs',
        {
            url: '/bugs',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.project.bugs.html',
            redirectTo: 'triage.dashboard.project.bugs.home'
        }).
        state('triage.dashboard.project.bugs.home',
        {
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.project.bugs.home.html',
            controller: 'triageDashboardProjectBugsHomeController'
        }).
        state('triage.dashboard.project.bugs.create',
        {
            url: '/create',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.project.bugs.create.html',
            controller: 'triageDashboardProjectBugsCreateController'
        }).
        state('triage.dashboard.project.bugs.edit',
        {
            url: '/edit/:bugid',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.project.bugs.edit.html',
            controller: 'triageDashboardProjectBugsEditController'
        }).
        state('triage.dashboard.project.users',
        {
            url: '/users',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.project.users.html',
            redirectTo: 'triage.dashboard.project.users.home'
        }).state('triage.dashboard.project.users.home',
        {
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.project.users.home.html',
            controller: 'triageDashboardProjectUsersHomeController'
        }).
        state('triage.dashboard.project.triage',
        {
            url: '/triage',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.project.triage.html',
            controller: 'triageDashboardProjectTriageController'
        }).
        state('triage.dashboard.project.closed',
        {
            url: '/closed',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/triage/templates/triage.dashboard.project.closed.html',
            controller: 'triageDashboardProjectClosedController'
        });
    $urlRouterProvider.otherwise('/dashboard/mywork');
}]);

triage.run(['$rootScope', 'utils', '$state', function ($rootScope, utils, $state)
{
    $rootScope.serverDetails = serverDetails;
    $rootScope.authToken = authToken;
    $rootScope.user = user;

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState)
    {
        if (toState.redirectTo)
        {
            event.preventDefault();
            $state.go(toState.redirectTo, toParams);
        }
    });

}]);