triage.controller('triageDashboardMyworkController', ['$scope', '$http', '$rootScope', 'utils', 'portalServices','userProjects',
    function ($scope, $http, $rootScope, utils, portalServices,userProjects)
    {
        $scope.bugs = [];
        $scope.filters = {};
        portalServices.getUserBugs($scope);
        $scope.resetFilters = function()
        {
            $scope.filters = {};
        };
    }]);