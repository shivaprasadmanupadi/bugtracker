triage.controller('triageDashboardProjectBugsCreateController', ['$scope', '$http', '$rootScope', 'utils', '$stateParams', 'portalServices', function ($scope, $http, $rootScope, utils, $stateParams, portalServices)
{
    $scope.bug = {};
    $scope.bug.regression = false;
    $scope.createBug = function ()
    {
        portalServices.createBug($scope);
    };

}]);