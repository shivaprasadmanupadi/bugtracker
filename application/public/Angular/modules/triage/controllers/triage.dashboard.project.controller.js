triage.controller('triageDashboardProjectController', ['$scope', '$http', '$rootScope', 'utils', '$stateParams', 'portalServices', function ($scope, $http, $rootScope, utils, $stateParams, portalServices)
{
    $scope.currentProject = {};
    $scope.currentProjectId = $stateParams.projectid;

    portalServices.getProjectDetails($scope);
    portalServices.getProjectUsers($scope);

}]);