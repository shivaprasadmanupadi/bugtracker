triage.controller('triageDashboardProjectBugsHomeController', ['$scope', '$http', '$rootScope', 'utils', '$stateParams', 'portalServices', function ($scope, $http, $rootScope, utils, $stateParams, portalServices)
{
    $scope.bugs = [];
    $scope.pages = [];
    $scope.filters = {};

    $scope.getBugs = function (page)
    {
        $scope.currentPage = page;

        var query = "";

        if($scope.filters.severity)
        {
            query += "severity=" + $scope.filters.severity + "&";
        }
        if($scope.filters.workflow_queue)
        {
            query += "workflow=" + $scope.filters.workflow_queue + "&";
        }
        if($scope.filters.current_user)
        {
            query += "userid=" + $scope.filters.current_user + "&";
        }

        if($scope.filters.created_by)
        {
            query += "created_by=" + $scope.filters.created_by + "&";
        }

        if($scope.filters.category)
        {
            query += "category=" + $scope.filters.category + "&";
        }

        query += "page=" + page;
        portalServices.getBugs($scope, query);
    };

    $scope.getBugs(1);

}]);