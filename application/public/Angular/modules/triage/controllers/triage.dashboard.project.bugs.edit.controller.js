triage.controller('triageDashboardProjectBugsEditController', ['$scope', '$http', '$rootScope', 'utils', '$stateParams', '$state', 'portalServices', function ($scope, $http, $rootScope, utils, $stateParams, $state, portalServices)
{
    $scope.bug = {};
    $scope.newComment = {};
    $scope.metaData = {};
    $scope.getBug = function ()
    {
        portalServices.getBug($scope);
    };

    $scope.getBug();

    $scope.addComment = function ()
    {
        portalServices.addComment($scope);
    };

    $scope.uploadScreenshot = function (element)
    {
        portalServices.uploadScreenshot($scope, element);
    };

    $scope.assignBugToUser = function ()
    {
        portalServices.assignBugToUser($scope);
    };

    $scope.updateWorkflow = function ()
    {
        portalServices.updateWorkflow($scope);

    };

    $scope.rejectBug = function ()
    {
        portalServices.rejectBug($scope);
    };

    $scope.routeBug = function ()
    {
        portalServices.routeBug($scope);

    };

    $scope.bugVerified = function ()
    {
        portalServices.bugVerified($scope);

    };

    $scope.reopenBug = function ()
    {
        portalServices.reopenBug($scope);

    };

    $scope.saveBugDetails = function ()
    {
        portalServices.saveBugDetails($scope);
    };

    $scope.uploadAttachment = function (element)
    {
        portalServices.uploadAttachment($scope, element);
    };

    $scope.deleteScreenshot = function (screenshot)
    {
        portalServices.deleteScreenshot($scope, screenshot);
    };
}]);