registration.controller('registrationController', ['$scope', '$rootScope', '$http', 'utils', function ($scope, $rootScope, $http, utils)
{
    $scope.workspace = {};

    $scope.signup = function ()
    {
        if (!$scope.workspace.workspaceName)
        {
            utils.showError("please enter a valid organisation name");
            return;
        }

        if (utils.containsSpecialCharacters($scope.workspace.workspaceName))
        {
            utils.showError("Organisation name should not contain any special character's.");
            return;
        }

        if($scope.workspace.workspaceName.length < 5)
        {
            utils.showError("Organisation name should contain minimum of 5 character's.");
            return;
        }

        if($scope.workspace.workspaceName.length > 12)
        {
            utils.showError("Organisation name should not contain more than 12 character's.");
            return;
        }

        if (!$scope.workspace.name)
        {
            utils.showError("please enter a valid name");
            return;
        }

        if (!$scope.workspace.email)
        {
            utils.showError("please enter a valid email id");
            return;
        }

        if (!utils.isValidEmail($scope.workspace.email))
        {
            utils.showError("invalid email id");
            return;
        }

        if (!$scope.workspace.password)
        {
            utils.showError("please enter a valid password");
            return;
        }

        if ($scope.workspace.password.length < 6)
        {
            utils.showError("password should be minimum of 6 character's");
            return;
        }

        if ($scope.workspace.password != $scope.workspace.confirmPassword)
        {
            utils.showError("password's mismatch");
            return;
        }

        $http(
            {
                method: 'POST',
                data: $scope.workspace,
                url: $rootScope.serverDetails.api + '/workspace'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    utils.showSuccess("Account Created Successfully");
                    window.location = $rootScope.serverDetails.contextPath + '/portal/' + data.payload._id + '/login';
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                if (data.result.message.indexOf('MongoError - insertDocument') != -1)
                {
                    utils.showError("organisation already exists");
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).finally(function ()
            {
            });
    };

}]);