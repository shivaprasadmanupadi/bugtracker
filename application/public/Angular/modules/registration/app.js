var registration = angular.module('registration', ['ui.router', 'helpers']);

registration.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider)
{
    $stateProvider.state('registration',
        {
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/registration/templates/registration.html',
            controller: 'registrationController'
        });
    $urlRouterProvider.otherwise('/');
}]);

registration.run(['$rootScope', function ($rootScope)
{
    $rootScope.serverDetails = serverDetails;
}]);