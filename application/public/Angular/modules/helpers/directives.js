helpers.directive('formatDate', ['$filter', function ($filter)
{
    var directive = {};

    directive.link = function (scope, element, attrs)
    {
        $(element).html($filter('date')(attrs.date, 'dd-MMMM-yyyy'));
    };

    return directive;
}]);

helpers.directive('customTooltip', [function ()
{
    var directive = {};

    directive.link = function (scope, element, attrs)
    {
        $(element).hover(function ()
        {
            $(element).tooltip('show');
        }, function ()
        {
            $(element).tooltip('hide');
        });
    };

    return directive;
}]);

helpers.directive('pageNumber', [function ()
{
    var directive = {};

    directive.link = function (scope, element, attrs)
    {
        $(element).on('click',function(event)
        {
            $(element).parent().find('.pagenumbers').removeClass('active');
            $(element).addClass('active');
        })
    };

    return directive;
}]);