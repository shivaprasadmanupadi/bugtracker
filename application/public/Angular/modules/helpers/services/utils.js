helpers.factory('utils', ['$rootScope', '$timeout', function ($rootScope, $timeout)
{
    var utils = {};
    utils.storage = {};

    utils.showError = function (message)
    {
        $rootScope.error.message = message;
        $('#errorMessage').show();
        $timeout(function ()
        {
            $('#errorMessage').hide();
        }, 3000);
    };

    utils.showSuccess = function (message)
    {
        $rootScope.success.message = message;
        $('#successMessage').show();
        $timeout(function ()
        {
            $('#successMessage').hide();
        }, 3000);
    };

    utils.showErrorPopup = function (msg)
    {
        var config = {};
        config.title = "Error";
        config.message = msg;
        config.buttons = {};
        config.buttons.warninng = {
            label: "OK",
            className: "btn-warning"
        };
        bootbox.dialog(config);
    };

    utils.showSuccessPopup = function (msg)
    {
        var config = {};
        config.title = "Success";
        config.message = msg;
        config.buttons = {};
        config.buttons.success = {
            label: "OK",
            className: "btn-success"
        };
        bootbox.dialog(config);
    };

    utils.showConfirmationPopup = function (title,label,msg,type,callback)
    {
        var config = {};
        config.title = title;
        config.message = msg;
        config.buttons = {};

        if(type == 'delete')
        {
            config.buttons.danger = {
                label: label,
                className: "btn-danger",
                callback:callback
            };
        }

        bootbox.dialog(config);
    };

    utils.storage.put = function (key, value, isObject)
    {
        if (isObject)
        {
            value = JSON.stringify(value);
        }

        localStorage.setItem(key, value);
    };

    utils.storage.get = function (key, isObject)
    {
        if (isObject)
        {
            return JSON.parse(localStorage.getItem(key));
        }

        return localStorage.getItem(key);
    };

    utils.arraysDiff = function (arr1, arr2)
    {
        return _.filter(arr1, function (obj)
        {
            return !_.findWhere(arr2, obj);
        });
    };

    utils.containsSpecialCharacters = function(string)
    {
        return !/^[a-zA-Z0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/.test(string);
    };

    utils.isValidEmail = function (email)
    {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(email);
    };

    return utils;
}]);