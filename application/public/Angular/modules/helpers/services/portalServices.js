helpers.factory('portalServices', ['$q', '$rootScope', '$http', 'utils', '$stateParams', '$state', '$location', function ($q, $rootScope, $http, utils, $stateParams, $state, $location)
{
    var portalServices = {};

    portalServices.getBugs = function ($scope, query)
    {
        $http(
            {
                method: 'GET',
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs?' + query
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $scope.bugs = data.payload;
                    $scope.pages = [];
                    var count = Math.ceil(data.meta / 5);
                    for (var index = 0; index < count; index++)
                    {
                        $scope.pages.push(index + 1);
                    }
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    portalServices.getUserBugs = function ($scope)
    {
        $http(
            {
                method: 'GET',
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/users/' + $rootScope.user._id + '/bugs'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $scope.bugs = data.payload;
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    portalServices.createBug = function ($scope)
    {
        if (!$scope.bug.title)
        {
            utils.showError("please enter a valid bug title");
            return;
        }
        if (!$scope.bug.description)
        {
            utils.showError("please enter a valid bug description");
            return;
        }
        if (!$scope.bug.severity)
        {
            utils.showError("please select a valid bug severity");
            return;
        }

        if($scope.currentProject.settings.config.mandate_category_selection && !$scope.bug.category)
        {
            utils.showError("please select a category");
            return;
        }
        $http(
            {
                method: 'POST',
                data: $scope.bug,
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    utils.showSuccess('created successfully');
                    $state.go('^.edit',{'bugid':data.payload._id});
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    portalServices.getProjectDetails = function ($scope)
    {
        $http(
            {
                method: 'GET',
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $scope.currentProject = data.payload;
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    portalServices.getProjectUsers = function ($scope)
    {
        $http(
            {
                method: 'GET',
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/users'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $scope.projectUsers = data.payload;
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    portalServices.getBug = function ($scope)
    {
        $http(
            {
                method: 'GET',
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $scope.bug = data.payload;
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    portalServices.addComment = function ($scope)
    {
        if (!$scope.newComment.comment)
        {
            utils.showError('please enter a valid comment');
            return;
        }

        $http(
            {
                method: 'POST',
                data: $scope.newComment,
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid + '/comments'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $scope.bug.comments.push(data.payload);
                    $scope.newComment = {};
                    utils.showSuccess('Comment Added Successfully');
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    portalServices.uploadScreenshot = function ($scope, element)
    {
        var fd = new FormData();
        /* Sample element.files JS object in this case would be of the form 'FileList {0: File, length: 1, item: function}' */
        fd.append("screenshot", element.files[0]);
        $http.post($rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid + '/screenshots', fd,
            {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            })
            .success(function (data)
            {
                if (data.result.success)
                {
                    $scope.bug.screenshots.push(data.payload);
                }
                else
                {
                    utils.showError(data.result.message);
                }
            })
            .error(function (data, status)
            {
                utils.showError(data.result.message);
            })
            .finally(function ()
            {
            });
    };

    portalServices.uploadAttachment = function ($scope, element)
    {
        var fd = new FormData();
        /* Sample element.files JS object in this case would be of the form 'FileList {0: File, length: 1, item: function}' */
        fd.append("attachment", element.files[0]);
        $http.post($rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid + '/attachments', fd,
            {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            })
            .success(function (data)
            {
                if (data.result.success)
                {
                    $scope.bug.attachments.push(data.payload);
                }
                else
                {
                    utils.showError(data.result.message);
                }
            })
            .error(function (data, status)
            {
                utils.showError(data.result.message);
            })
            .finally(function ()
            {
            });
    };

    portalServices.assignBugToUser = function ($scope)
    {
        if (!$scope.metaData.current_user)
        {
            utils.showError('please select a user');
            return;
        }

        $http(
            {
                method: 'POST',
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid + '/assign/' + $scope.metaData.current_user
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $state.reload();
                    utils.showSuccess('Assigned Successfully');
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    portalServices.deleteScreenshot = function ($scope,screenshot)
    {
        $http(
            {
                method: 'DELETE',
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid + '/screenshots/' + screenshot._id
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    utils.showSuccess('Screenshot Deleted Successfully');
                    $state.reload();
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    portalServices.updateWorkflow = function ($scope)
    {
        if (!$scope.bug.perform_user)
        {
            utils.showError('please select a valid perform user');
            return;
        }
        if (!$scope.bug.should_be_verified_by)
        {
            utils.showError('please select a valid user to verify the bug');
            return;
        }

        var payload = {};
        payload.perform_user = $scope.bug.perform_user;
        payload.should_be_verified_by = $scope.bug.should_be_verified_by;
        $http(
            {
                method: 'POST',
                data: payload,
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid + '/workflow'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $state.reload();
                    utils.showSuccess('Updated Successfully');
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });

    };

    portalServices.rejectBug = function ($scope)
    {
        var payload = {};
        payload.rejectionMessage = $scope.metaData.rejectionMessage;
        $http(
            {
                method: 'POST',
                data: payload,
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid + '/reject'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $state.reload();
                    utils.showSuccess('Updated Successfully');
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    portalServices.routeBug = function ($scope)
    {
        var payload = {};
        payload.resolution_status = $scope.bug.resolution_status;
        payload.fixed_in_build = $scope.bug.fixed_in_build;
        payload.resolution_message = $scope.bug.resolution_message;

        if (!payload.resolution_status)
        {
            utils.showError('invalid resolution status');
            return;
        }

        $http(
            {
                method: 'POST',
                data: payload,
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid + '/route'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $state.reload();
                    utils.showSuccess('Updated Successfully');
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });

    };

    portalServices.bugVerified = function ($scope)
    {
        var payload = {};
        payload.verificationMessage = $scope.metaData.verificationMessage;

        if (!payload.verificationMessage)
        {
            utils.showError('invalid verification Message');
            return;
        }

        $http(
            {
                method: 'POST',
                data: payload,
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid + '/verified'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $state.reload();
                    utils.showSuccess('Updated Successfully');
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });

    };

    portalServices.setSection = function($scope)
    {
        $scope.$on('$viewContentLoaded', function(){
            $('a#section_' + $location.search().section).trigger('click');
        });
    };

    portalServices.reopenBug = function ($scope)
    {
        var payload = {};
        payload.reopenMessage = $scope.metaData.reopenMessage;

        $http(
            {
                method: 'POST',
                data: payload,
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid + '/reopen'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $state.reload();
                    utils.showSuccess('Updated Successfully');
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });

    };

    portalServices.saveBugDetails = function ($scope)
    {
        if (!$scope.bug.title)
        {
            utils.showError('invalid title');
            return;
        }

        if (!$scope.bug.description)
        {
            utils.showError('invalid description');
            return;
        }

        if (!$scope.bug.severity)
        {
            utils.showError('invalid severity');
            return;
        }

        if($scope.currentProject.settings.config.mandate_category_selection && !$scope.bug.category)
        {
            utils.showError("please select a category");
            return;
        }

        if($scope.currentProject.settings.config.mandate_release_selection && !$scope.bug.target_release)
        {
            utils.showError("please select a target release");
            return;
        }

        var payload = {};
        payload.title = $scope.bug.title;
        payload.description = $scope.bug.description;
        payload.found_in_build = $scope.bug.found_in_build;
        payload.severity = $scope.bug.severity;
        payload.regression = $scope.bug.regression;
        payload.steps_to_reproduce = $scope.bug.steps_to_reproduce;
        payload.category = $scope.bug.category;
        payload.target_release = $scope.bug.target_release;

        $http(
            {
                method: 'PUT',
                data: payload,
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/bugs/' + $stateParams.bugid
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $state.reload();
                    utils.showSuccess('Updated Successfully');
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });

    };

    return portalServices;
}]);

helpers.service('workspaceProjects', ['$rootScope', 'utils', '$q', '$http', function ($rootScope, utils, $q, $http)
{
    var deferred = $q.defer();
    $http(
        {
            method: 'GET',
            url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects'
        }).success(function (data, status, headers, config)
        {
            if (data.result.success)
            {
                $rootScope.projects = data.payload;
                deferred.resolve($rootScope.projects);
            }
            else
            {
                utils.showError(data.result.message);
                deferred.reject(data);
            }
        }).error(function (data, status, headers, config)
        {
            utils.showError(data.result.message);
            deferred.reject(data);
        }).finally(function ()
        {
        });
    return deferred.promise;
}]);

helpers.service('userProjects', ['$rootScope', 'utils', '$q', '$http', function ($rootScope, utils, $q, $http)
{
    var deferred = $q.defer();
    $http(
        {
            method: 'GET',
            url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/users/' + $rootScope.user._id + '/projects'
        }).success(function (data, status, headers, config)
        {
            if (data.result.success)
            {
                $rootScope.projects = data.payload;
                deferred.resolve($rootScope.projects);
            }
            else
            {
                utils.showError(data.result.message);
                deferred.reject(data);
            }
        }).error(function (data, status, headers, config)
        {
            utils.showError(data.result.message);
            deferred.reject(data);
        }).finally(function ()
        {
        });
    return deferred.promise;
}]);

helpers.service('workspaceUsers', ['$rootScope', 'utils', '$q', '$http', function ($rootScope, utils, $q, $http)
{
    var deferred = $q.defer();
    $http(
        {
            method: 'GET',
            url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/users'
        }).success(function (data, status, headers, config)
        {
            if (data.result.success)
            {
                $rootScope.users = data.payload;
                deferred.resolve($rootScope.users);
            }
            else
            {
                utils.showError(data.result.message);
                deferred.reject(data);
            }
        }).error(function (data, status, headers, config)
        {
            utils.showError(data.result.message);
            deferred.reject(data);
        }).finally(function ()
        {
        });


    return deferred.promise;
}]);