var admin = angular.module('admin', ['ui.router', 'helpers']);

admin.factory('requestInterceptor', ['$rootScope', function ($rootScope)
{
    var requestInterceptor = {};

    requestInterceptor.request = function (config)
    {
        config.headers['Authorization'] = $rootScope.authToken;
        return config;
    };

    return requestInterceptor;
}]);

admin.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider)
{
    $httpProvider.interceptors.push('requestInterceptor');

    $stateProvider.state('admin',
        {
            abstract: true,
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.html'
        }).
        state('admin.dashboard',
        {
            abstract: true,
            url: 'dashboard',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.html'
        }).
        state('admin.dashboard.home',
        {
            url: '/home',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.home.html',
            controller: 'adminDashboardHomeController'
        }).
        state('admin.dashboard.usermanagement',
        {
            url: '/usermanagement',
            template: '<ui-view></ui-view>',
            redirectTo:'admin.dashboard.usermanagement.home'
        }).
        state('admin.dashboard.usermanagement.home',
        {
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.usermanagement.home.html',
            controller: 'adminDashboardUsermanagementHomeController'
        }).
        state('admin.dashboard.usermanagement.user',
        {
            url: '/user/:id',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.usermanagement.user.html',
            controller: 'adminDashboardUsermanagementUserController'
        }).
        state('admin.dashboard.mywork',
        {
            url: '/mywork',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.mywork.html',
            controller: 'adminDashboardMyworkController'
        }).
        state('admin.dashboard.project',
        {
            url: '/project/:projectid',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.project.html',
            controller: 'adminDashboardProjectController',
             resolve:{
             currentProject:function($stateParams,$q,$http,$rootScope,utils)
             {
             var deferred = $q.defer();

             $http(
             {
             method : 'GET' ,
             url : $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $stateParams.projectid
             }).success(function(data , status , headers , config)
             {
             if (data.result.success)
             {
             deferred.resolve(data.payload);
             }
             else
             {
             utils.showError(data.result.message);
             deferred.reject(data.payload);
             }
             }).error(function(data , status , headers , config)
             {
             utils.showError(data.result.message);
             deferred.reject(data.payload);
             }).finally(function()
             {
             });

             return deferred.promise;
             }
             }
        }).
        state('admin.dashboard.project.bugs',
        {
            url: '/bugs',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.project.bugs.html',
            redirectTo: 'admin.dashboard.project.bugs.home'
        }).
        state('admin.dashboard.project.bugs.home',
        {
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.project.bugs.home.html',
            controller: 'adminDashboardProjectBugsHomeController'
        }).
        state('admin.dashboard.project.bugs.create',
        {
            url: '/create',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.project.bugs.create.html',
            controller: 'adminDashboardProjectBugsCreateController'
        }).
        state('admin.dashboard.project.bugs.edit',
        {
            url: '/edit/:bugid',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.project.bugs.edit.html',
            controller: 'adminDashboardProjectBugsEditController'
        }).
        state('admin.dashboard.project.users',
        {
            url: '/users',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.project.users.html',
            redirectTo: 'admin.dashboard.project.users.home'
        }).state('admin.dashboard.project.users.home',
        {
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.project.users.home.html',
            controller: 'adminDashboardProjectUsersHomeController'
        }).
        state('admin.dashboard.project.triage',
        {
            url: '/triage',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.project.triage.html',
            controller: 'adminDashboardProjectTriageController'
        }).
        state('admin.dashboard.project.closed',
        {
            url: '/closed',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.project.closed.html',
            controller: 'adminDashboardProjectClosedController'
        }).
        state('admin.dashboard.project.settings',
        {
            url: '/settings',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/admin/templates/admin.dashboard.project.settings.html',
            controller: 'adminDashboardProjectSettingsController'
        });
    $urlRouterProvider.otherwise('/dashboard/home');
}]);

admin.run(['$rootScope', 'utils', '$state', function ($rootScope, utils, $state)
{
    $rootScope.serverDetails = serverDetails;
    $rootScope.authToken = authToken;
    $rootScope.user = user;

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState)
    {
        if (toState.redirectTo)
        {
            event.preventDefault();
            $state.go(toState.redirectTo, toParams);
        }
    });

}]);