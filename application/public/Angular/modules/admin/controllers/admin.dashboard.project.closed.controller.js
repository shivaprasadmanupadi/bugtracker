admin.controller('adminDashboardProjectClosedController', ['$scope', '$http', '$rootScope', 'utils', '$stateParams', 'portalServices', function ($scope, $http, $rootScope, utils, $stateParams, portalServices)
{
    $scope.bugs = [];
    $scope.pages = [];

    $scope.getClosedBugs = function (page)
    {
        $scope.currentPage = page;
        var query = "status=closed&page=" + page;
        portalServices.getBugs($scope, query);
    };

    $scope.getClosedBugs(1);
}]);