admin.controller('adminDashboardProjectController', ['$scope', '$http', '$rootScope', 'utils', '$stateParams', 'portalServices','currentProject', function ($scope, $http, $rootScope, utils, $stateParams, portalServices, currentProject)
{
    $scope.currentProject = currentProject;
    $scope.currentProjectId = $stateParams.projectid;

   //portalServices.getProjectDetails($scope);
    portalServices.getProjectUsers($scope);

}]);