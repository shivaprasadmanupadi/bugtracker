admin.controller('adminDashboardMyworkController', ['$scope', '$http', '$rootScope', 'utils', 'workspaceProjects', 'portalServices','workspaceProjects', function ($scope, $http, $rootScope, utils, workspaceProjects, portalServices,workspaceProjects)
{
    $scope.bugs = [];
    $scope.filters = {};
    portalServices.getUserBugs($scope);
    $scope.resetFilters = function()
    {
        $scope.filters = {};
    };

}]);