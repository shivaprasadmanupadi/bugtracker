admin.controller('adminDashboardUsermanagementHomeController', ['$scope', '$http', '$rootScope', 'utils', 'portalServices', 'workspaceUsers', function ($scope, $http, $rootScope, utils, portalServices, workspaceUsers)
{
    $scope.newUser = {};
    $scope.metaData = {};

    $scope.createUser = function ()
    {
        if (!$scope.newUser.name)
        {
            utils.showError("please enter a valid name");
            return;
        }
        if (!$scope.newUser.email)
        {
            utils.showError("please enter a valid email");
            return;
        }
        if (!$scope.newUser.role)
        {
            utils.showError("please select a valid role");
            return;
        }
        if (!$scope.newUser.password)
        {
            utils.showError("please enter a valid password");
            return;
        }

        for (var index = 0; index < $rootScope.users.length; index++)
        {
            if ($rootScope.users[index].email.toLowerCase() == $scope.newUser.email.toLowerCase())
            {
                utils.showError("User with the email " + $scope.newUser.email + " already exists.");
                return;
            }
        }

        $http(
            {
                method: 'POST',
                data: $scope.newUser,
                url: $rootScope.serverDetails.api + '/workspace/' + $scope.user.workspace_id + '/users'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $rootScope.users.push(data.payload);
                    $('#addUserModal').modal('hide');
                    utils.showSuccess("Created Successfully");
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

}]);