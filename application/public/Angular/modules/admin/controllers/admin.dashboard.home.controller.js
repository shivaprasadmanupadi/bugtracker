admin.controller('adminDashboardHomeController', ['$scope', '$http', '$rootScope', 'utils', 'workspaceProjects', function ($scope, $http, $rootScope, utils, workspaceProjects)
{
    $scope.newProject = {};
    $scope.metaData = {};

    $scope.createProject = function ()
    {
        if (!$scope.newProject.name)
        {
            utils.showError("please enter a valid project name");
            return;
        }

        for (var index = 0; index < $rootScope.projects.length; index++)
        {
            if ($rootScope.projects[index].name.toLowerCase() == $scope.newProject.name.toLowerCase())
            {
                utils.showError("Project with the name " + $scope.newProject.name + " already exists.");
                return;
            }
        }

        $http(
            {
                method: 'POST',
                data: $scope.newProject,
                url: $rootScope.serverDetails.api + '/workspace/' + $scope.user.workspace_id + '/projects'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $scope.projects.push(data.payload);
                    $scope.newProject = {};
                    utils.showSuccess("Created Successfully");
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

}]);