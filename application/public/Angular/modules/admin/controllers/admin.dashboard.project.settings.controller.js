admin.controller('adminDashboardProjectSettingsController', ['$scope', '$http', '$rootScope', 'utils', '$stateParams', 'portalServices', function ($scope, $http, $rootScope, utils, $stateParams, portalServices)
{
    $scope.newCategory = {};
    $scope.newRelease = {};
    $scope.metaData = {};
    $scope.currentProjectClone = angular.copy($scope.currentProject);

    $scope.addCategory = function()
    {
        $http(
        {
            method: 'POST',
            data: $scope.newCategory,
            url: $rootScope.serverDetails.api + '/workspace/' + $scope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/settings/categories'
        }).success(function (data, status, headers, config)
        {
            if (data.result.success)
            {
            }
            else
            {
                utils.showError(data.result.message);
            }
        }).error(function (data, status, headers, config)
        {
            utils.showError(data.result.message);
        }).finally(function ()
        {
        });
    };

    $scope.addRelease = function()
    {
        $http(
            {
                method: 'POST',
                data: $scope.newRelease,
                url: $rootScope.serverDetails.api + '/workspace/' + $scope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/settings/releases'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    $scope.mandateCategorySelection = function()
    {
        if($scope.currentProjectClone.settings.config.mandate_category_selection)
        {
            $scope.currentProjectClone.settings.config.enable_categories = true;
        }
    };

    $scope.enableOrDisableCategories = function()
    {
        if(!$scope.currentProjectClone.settings.config.enable_categories)
        {
            $scope.currentProjectClone.settings.config.mandate_category_selection = false;
        }
    };

    $scope.mandateReleaseSelection = function()
    {
        if($scope.currentProjectClone.settings.config.mandate_release_selection)
        {
            $scope.currentProjectClone.settings.config.enable_releases = true;
        }
    };

    $scope.enableOrDisableReleases = function()
    {
        if(!$scope.currentProjectClone.settings.config.enable_releases)
        {
            $scope.currentProjectClone.settings.config.mandate_release_selection = false;
        }
    };

    $scope.saveSettingsconfiguration = function()
    {
        $http(
            {
                method: 'POST',
                data: $scope.currentProjectClone.settings.config,
                url: $rootScope.serverDetails.api + '/workspace/' + $scope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/settings/configuration'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };


}]);