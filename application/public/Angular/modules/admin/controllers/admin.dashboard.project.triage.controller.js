admin.controller('adminDashboardProjectTriageController', ['$scope', '$http', '$rootScope', 'utils', '$stateParams', 'portalServices', function ($scope, $http, $rootScope, utils, $stateParams, portalServices)
{
    $scope.bugs = [];
    $scope.pages = [];
    $scope.filters = {};

    $scope.getTriagedBugs = function (page)
    {
        $scope.currentPage = page;
        var query = "workflow=triage&page=" + page + "&";

        if($scope.filters.severity)
        {
            query += "severity=" + $scope.filters.severity + "&";
        }
        if($scope.filters.created_by)
        {
            query += "created_by=" + $scope.filters.created_by + "&";
        }

        portalServices.getBugs($scope, query);
    };
    $scope.getTriagedBugs(1);
}]);