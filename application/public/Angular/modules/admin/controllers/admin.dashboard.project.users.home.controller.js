admin.controller('adminDashboardProjectUsersHomeController', ['$scope', '$http', '$rootScope', 'utils', '$stateParams', 'portalServices', 'workspaceUsers', function ($scope, $http, $rootScope, utils, $stateParams, portalServices, workspaceUsers)
{
    $scope.unassociatedUsers = [];
    $scope.metaData = {};

    $scope.getUsers = function ()
    {
        workspaceUsers.then(function (users)
        {
            $scope.unassociatedUsers = utils.arraysDiff(users, $scope.projectUsers);
            $('#associateUserModal').modal('show');
        });
    };

    $scope.associateUser = function (user)
    {
        $http(
            {
                method: 'POST',
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/users/' + user._id
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $scope.projectUsers.push(user);
                    utils.showSuccess(user.name + ' Added Successfully');
                    $('#associateUserModal').modal('hide');
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    $scope.deleteUserFromProject = function()
    {
        $http(
            {
                method: 'DELETE',
                url: $rootScope.serverDetails.api + '/workspace/' + $rootScope.user.workspace_id + '/projects/' + $scope.currentProjectId + '/users/' + $scope.deleteThisUser._id + '?userid=' + $scope.metaData.newSelectedUser
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    $scope.projectUsers = utils.arraysDiff($scope.projectUsers, [$scope.deleteThisUser]);
                    $('#associateUserModal').modal('hide');
                    utils.showSuccess($scope.deleteThisUser.name + ' Unlinked Successfully');
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    $scope.dissociateUserConfirmation = function ()
    {
        if(!$scope.metaData.newSelectedUser)
        {
            utils.showError('please select a valid user');
            return;
        }

        if($scope.metaData.newSelectedUser == $scope.deleteThisUser._id)
        {
            utils.showError('please select a another user');
            return;
        }

        utils.showConfirmationPopup('Delete user','Delete','Are you sure , you want to delete the user "' + $scope.deleteThisUser.name + '" from this project ?','delete',$scope.deleteUserFromProject);
        $('#selectAnotherUser').modal('hide');
    };

    $scope.selectAnotherUser = function(user)
    {
        $scope.deleteThisUser = user;
        $('#selectAnotherUser').modal('show');
    };

}]);