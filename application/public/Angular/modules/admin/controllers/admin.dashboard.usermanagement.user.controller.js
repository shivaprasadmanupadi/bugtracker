admin.controller('adminDashboardUsermanagementUserController', ['$scope', '$http', '$rootScope', 'utils', 'portalServices', '$stateParams', function ($scope, $http, $rootScope, utils, portalServices, $stateParams)
{
    $scope.userId = $stateParams.id;
    $scope.currentUser = {};

    $scope.getUser = function(userId)
    {
        $http(
            {
                method: 'GET',
                url: $rootScope.serverDetails.api + '/workspace/' + $scope.user.workspace_id + '/users/' + userId + '?projects=true'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    console.log(data.payload);
                    $scope.currentUser = data.payload;
                }
                else
                {
                    utils.showError(data.result.message);
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError(data.result.message);
            }).finally(function ()
            {
            });
    };

    $scope.getUser($scope.userId);
}]);