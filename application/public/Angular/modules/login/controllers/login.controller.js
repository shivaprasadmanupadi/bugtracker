login.controller('loginController', ['$scope', '$rootScope', '$http', 'utils', function ($scope, $rootScope, $http, utils)
{
    $scope.user = {};
    $scope.user.workspaceid = $rootScope.workspace._id;

    $scope.login = function ()
    {
        if (!$scope.user.email)
        {
            utils.showError("please enter a valid email id");
            return;
        }

        if (!$scope.user.password)
        {
            utils.showError("please enter a valid password");
            return;
        }

        $http(
            {
                method: 'POST',
                data: $scope.user,
                url: $rootScope.serverDetails.api + '/workspace/auth/login'
            }).success(function (data, status, headers, config)
            {
                if (data.result.success)
                {
                    window.location
                        = $rootScope.serverDetails.contextPath + '/portal/' + data.payload.workspace_id + "/" + data.payload.role + '/console';
                }
                else
                {
                    utils.showError('invalid credentials');
                }
            }).error(function (data, status, headers, config)
            {
                utils.showError('invalid credentials');
            }).finally(function ()
            {
            });
    };

}]);