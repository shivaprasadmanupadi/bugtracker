var login = angular.module('login', ['ui.router', 'helpers']);

login.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider)
{
    $stateProvider.state('login',
        {
            url: '/',
            templateUrl: serverDetails.contextPath + '/resources/Angular/modules/login/templates/login.html',
            controller: 'loginController'
        });
    $urlRouterProvider.otherwise('/');
}]);

login.run(['$rootScope', function ($rootScope)
{
    $rootScope.serverDetails = serverDetails;
    $rootScope.workspace = workspace;
}]);