var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema(
	{
		name        : {
			type    : String,
			required: true
		},
		email       : {
			type    : String,
			required: true,
		},
		password    : {
			type    : String,
			required: true
		},
		created_at  : {
			type    : Date,
			required: true,
			default : Date.now
		},
		updated_at  : {
			type    : Date,
			required: true,
			default : Date.now
		},
		workspace_id: {
			type    : mongoose.Schema.Types.ObjectId,
			required: true,
			ref     : 'Workspace'
		},
		role        : {
			type    : String,
			required: true
		}
	});

userSchema.methods.validatePassword = function (password)
{
	return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', userSchema);