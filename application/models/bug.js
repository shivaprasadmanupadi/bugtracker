var mongoose = require('mongoose');

var bugSchema = mongoose.Schema(
    {
        //workflow
        workspace_id: {
            required: true,
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Workspace'
        },
        project_id: {
            required: true,
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Project'
        },
        title: {
            required: true,
            type: String
        },
        identifier: {
            required: true,
            type: String
        },
        description: {
            type: String
        },
        workflow_queue: {
            required: true,
            type: String,
            default: 'triage'
        },
        status: {
            required: true,
            type: String,
            default: 'open'
        },
        severity: {
            required: true,
            type: String,
            default: 'high'
        },
        regression: {
            type: Boolean,
            default: false
        },
        created_at: {
            required: true,
            type: Date,
            default: Date.now
        },
        updated_at: {
            required: true,
            type: Date,
            default: Date.now
        },
        closed_at: {
            type: Date
        },
        current_user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        perform_user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        should_be_verified_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        verified_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        screenshots: [{
            type: mongoose.Schema.Types.Mixed //by,filename,on
        }],
        attachments: [{
            type: mongoose.Schema.Types.Mixed //by,filename,on
        }],
        created_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        updated_by: {
            type: String
        },
        comments: [{
            type: mongoose.Schema.Types.Mixed //by,comment,on
        }],
        activity_log: [{
            type: String
        }],
        found_in_build: {
            type: String
        },
        fixed_in_build: {
            type: String
        },
        steps_to_reproduce: {
            type: String
        },
        resolution_status: {
            type: String
        },
        category: {
            type: mongoose.Schema.Types.ObjectId
        },
        target_release: {
            type: mongoose.Schema.Types.ObjectId
        }
    }
);

bugSchema.pre('update', function (next)
{
    this.updated_at = new Date();
    next();
});

module.exports = mongoose.model('Bug', bugSchema);