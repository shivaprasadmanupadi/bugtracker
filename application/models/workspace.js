var mongoose = require('mongoose');

var workspaceSchema = mongoose.Schema(
	{
		name      : {
			type    : String,
			required: true,
			unique  : true
		},
		created_at: {
			type    : Date,
			required: true,
			default : Date.now
		},
		updated_at: {
			type    : Date,
			required: true,
			default : Date.now
		},
		type      : {
			type    : String,
			required: true,
			default : "trial"
		}
	});

module.exports = mongoose.model('Workspace', workspaceSchema);