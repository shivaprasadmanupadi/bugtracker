var mongoose = require('mongoose');

var categorySchema = mongoose.Schema(
	{
		name	:{
			type    : String,
			required: true
		},
		created_at  : {
			required: true,
			type    : Date,
			default : Date.now
		}
	}
);

var releaseSchema = mongoose.Schema(
	{
		name	:{
			type    : String,
			required: true
		},
		date  : {
			required: true,
			type    : Date
		}
	}
);

var projectSchema = mongoose.Schema(
	{
		name        : {
			required: true,
			type    : String
		},
		created_at  : {
			required: true,
			type    : Date,
			default : Date.now
		},
		updated_at  : {
			required: true,
			type    : Date,
			default : Date.now
		},
		workspace_id: {
			required: true,
			type    : mongoose.Schema.Types.ObjectId,
			ref     : 'Workspace'
		},
		users       : [{
			type: mongoose.Schema.Types.ObjectId,
			ref : 'User'
		}],
		settings: {
			config:{
				enable_categories:
				{
					type:mongoose.Schema.Types.Boolean,
					default: true
				},
				enable_releases:
				{
					type:mongoose.Schema.Types.Boolean,
					default: true
				},
				mandate_category_selection:
				{
					type:mongoose.Schema.Types.Boolean,
					default: false
				},
				mandate_release_selection:
				{
					type:mongoose.Schema.Types.Boolean,
					default: false
				}
			},
			categories:[categorySchema],
			releases:[releaseSchema]
		}
	}
);

module.exports = mongoose.model('Project', projectSchema);